import os
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .models import *
from .forms import RegistrationForm, EditProfileForm
from django.conf import settings
from django.template.context_processors import media
from django.core.files.storage import default_storage

def index(request):
    title = "aMedia Buzz"
    return render(request, 'index.html' , {'title': title})


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.userprofile.mobile_no = form.cleaned_data.get('mobile_no')
            user.userprofile.email_id = form.cleaned_data.get('email_id')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegistrationForm()
    return render(request, 'register.html', {'form': form})


@login_required
def home(request):
    title = "aMedia Buzz"
    return render(request, 'home.html' , {'title': title})

@login_required
def fbAccess(request):
    title = "aMedia Buzz"
    return render(request, 'fb_access.html' , {'title': title})

@login_required
def gAccess(request):
    title = "aMedia Buzz"
    return render(request, 'g_access.html' , {'title': title})

@login_required
def tAccess(request):
    title = "aMedia Buzz"
    return render(request, 't_access.html' , {'title': title})

@login_required
def bAccess(request):
    title = "aMedia Buzz"
    return render(request, 'b_access.html' , {'title': title})





@login_required
def feature(request):
    title = "aMedia Buzz"
    return render(request, 'feature.html' , {'title': title})

@login_required
def usage(request):
    title = "aMedia Buzz"
    return render(request, 'usage.html' , {'title': title})



