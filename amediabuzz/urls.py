"""amediabuzz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from user import views as core_views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^amediabuzz.com/$', core_views.index, name='index'),
    
    url(r'^$', core_views.home, name='home'),
    url(r'^fbook_access/$', core_views.fbAccess, name='fb_access'),
    url(r'^t_access/$', core_views.tAccess, name='t_access'),
    url(r'^g_access/$', core_views.gAccess, name='g_access'),
    url(r'^b_access/$', core_views.bAccess, name='b_access'),
    
    url(r'^features/$', core_views.feature, name='feature'),
    url(r'^analytics_usages/$', core_views.usage, name='usage'),
    
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
    url(r'^register/$', core_views.register, name='register'),
]
